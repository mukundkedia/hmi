import React, { useState } from 'react'
import ChargeStatus from './ChargeStatus/ChargeStatus';

const  Charging = () =>{

    const [charge, setCharge] = useState(45);
    const [chargeStatus, setChargeStatus] = useState(1);

        return (
            <div style={{textAlign:"center"}}>
                <h1>Charge : {charge}%</h1>
                <h1>Charging : <ChargeStatus status={chargeStatus}/></h1>
            </div>
        )
    }

export default Charging

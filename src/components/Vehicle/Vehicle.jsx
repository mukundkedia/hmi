import React, { useState,useEffect } from 'react'
import axios from 'axios';

const Vehicle = () => {

    const [speed,setSpeed] = useState(55);
    const [status, setStatus] = useState('P');
    const [odometer,setOdometer] = useState(67);
    const [drivetime,setDrivetime] = useState(435677);
    const [ac,setAc] = useState(0); //

    // useEffect( () => {
    //     fetch('http://paraspravaig.pythonanywhere.com/dict')
    //     .then(response => response.json())
    //     .then(data => console(data.odometer))
        
    //   });

    let time={};

    if(drivetime>0){
        time = {
            hours: Math.floor((drivetime / (1000 * 60 * 60)) % 24),
            minutes: Math.floor((drivetime / 1000 / 60) % 60),
            seconds: Math.floor((drivetime / 1000) % 60)
          };
    }

    return (
        <div style={{textAlign:"center"}}>
        <h1>Speed : {speed} km/h</h1>
        <h1>Car Status : {status} </h1>
        <h1>Odometer : {odometer}</h1>
        <h1>Drive Time : {time.hours}hrs {time.minutes}mins {time.seconds}secs</h1>
        <h1>Air Condition <button onClick={()=>{setAc(1-ac)}}>On/Off</button>  {ac}</h1>
        </div>
    )
}

export default Vehicle
